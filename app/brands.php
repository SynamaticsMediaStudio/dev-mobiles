<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brands extends Model
{
    public function devices()
    {
        return $this->hasMany("App\devices","brand","id");
    }
}
