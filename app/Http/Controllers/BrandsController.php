<?php

namespace App\Http\Controllers;

use App\brands;
use App\devices;
use SimpleXMLElement;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = brands::all();
        return view("brands.index",['brands'=>$brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_time_limit(0);
        $url = "https://www.devicespecifications.com/en/brand-more";
        $client = new Client();
        $result = $client->get($url);
        $body = $result->getBody();
        $match = "";
        if (preg_match('#<div class="brand-listing-container-news"[^>]*>(.*?)</div>#', $body, $match) == 1) {
            $match = $match[1];
        }
        $links = [];
        preg_match_all('#<a[^>]*>(.*?)</a>#', $match, $alinks);
        foreach ($alinks[0] as $key) {
            $a  = new SimpleXMLElement($key);
            $links[] = ['brand'=>$a->__toString(),"hyperlink"=>$a['href']->__toString()];
        }
        foreach ($links as $key) {
            $app = brands::where("title",$key['brand'])->first();
            if(!$app){
                $appnew = new brands;
                $appnew->title = $key['brand'];
                $appnew->url = $key['hyperlink'];
                $appnew->save();
            }
        }
        return redirect()->route('brands.index')->with(['status'=>"Database Updated!"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function show(brands $brands,$id)
    {
        $brand = $brands->findorfail($id);
        return view("brands.show",["brand"=>$brand]);
    }

    /**
     * Download the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function download(devices $devices,$brand)
    {
        if($brand == "all"){
            $products = $devices::with('brand')->get();
        }
        elseif($brand == "updated"){
            $products = $devices::with('brand')->where('downloaded',0)->get();
        }
        else{
            $products = $devices::with('brand')->where('brand',$brand)->get();
        }
       $csvExporter = new \Laracsv\Export();
       $csvExporter->beforeEach(function ($product) {
           $product->downloaded = 1;
           $product->save();
           $product->brand = $product->Brand->title;
        });
        $csvExporter->build($products,[
            'name'=>"Model",
            'brand'=>"Brand",
            'url'=>"Link",
            'dimentions'=>"Dimentions",
            'weight'=>"Weight",
            'soc'=>"Socket",
            'cpu'=>"CPU",
            'gpu'=>"GPU",
            'ram'=>"RAM",
            'storage'=>"Storage",
            'memory'=>"Memory Card",
            'display'=>"Display",
            'battery'=>"Battery",
            'os'=>"OS",
            'camera'=>"Camera",
            'wifi'=>"WiFi",
            'usb'=>"USB",
            'bluetooth'=>"Bluetooth",
            'positioning'=>"Positioning",
            ]);
       $csvExporter->download();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function edit(brands $brands)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, brands $brands)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function destroy(brands $brands)
    {
        //
    }
}
