<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Artisan;
use DB;
use Hash;

class Installation extends Controller
{
    /**
     * Check Installation.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("installer");
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Artisan::call('key:generate');
        $request->validate([
            "name"=>"required",
            "url"=>"required",
            "hostname"=>"required",
            "database"=>"required",
            "username"=>"required",
            "db_password"=>"required",
        ]);
            $file_contents = file_get_contents(base_path().'/.env');
            $file_contents = str_replace("DB_HOST=127.0.0.1","DB_HOST=".$request->hostname,$file_contents);
            $file_contents = str_replace("DB_DATABASE=homestead","DB_DATABASE=".$request->database,$file_contents);
            $file_contents = str_replace("DB_USERNAME=homestead","DB_USERNAME=".$request->username,$file_contents);
            $file_contents = str_replace("DB_PASSWORD=secret","DB_PASSWORD=".$request->db_password,$file_contents);
            $file_contents = str_replace("APP_URL=http://localhost","APP_URL=".$request->url,$file_contents);
            $file_contents = str_replace("APP_NAME=Laravel",'APP_NAME="'.$request->name.'"',$file_contents);
            $file_contents = str_replace("APP_ENV=local","APP_ENV=production",$file_contents);
            $fle = fopen(base_path()."/.env","w");
            fwrite($fle, $file_contents);
            fclose($fle);
            return redirect()->route('install.create',["email"=>$request->email,"password"=>Hash::make($request->password)]);
        }
    public function update(Request $request)
    {
        define('STDIN',fopen("php://stdin","r"));
        Artisan::call('migrate');
        // $path = base_path();
        // // exec("cd $path",$outut);
        // exec("cd $path && php artisan migrate",$outut);
        // return $outut;
        // return User::create([
        //         'name' => "Administrator",
        //         'email' => $request->email,
        //         'password' => $request->password,
        //     ]);
        }
}
