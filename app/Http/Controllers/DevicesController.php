<?php

namespace App\Http\Controllers;

use App\devices;
use App\brands;
use SimpleXMLElement;
use DOMDocument;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class DevicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = devices::all();
        return view("devices.index",["devices"=>$devices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_time_limit(0);
        $red = brands::all();
        foreach ($red as $brand) {
            $client = new Client();
            $result = $client->get($brand->url);
            $body = html_entity_decode($result->getBody(), 0, 'UTF-8');
            $devices = [];
            preg_match_all('#<div id="model_[^>]*>(.*?)</div>#', $body, $match);
            foreach ($match[0] as $key) {
                preg_match_all ("/<h3><a .*?>([^`]*?)<\/a><\/h3>/", $key, $matches);
                preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $key, $url);
                $devices[] = ['device'=>strip_tags($matches[0][0]),"hyperlink"=>strip_tags($url[0][2])];
            }
            $caklc = [];
            foreach ($devices as $key) {
                $caklc[$key['device']] = ["brand"=>$brand->id,"device"=>$key['device'],"hyperlink"=>$key['hyperlink']];
                $checkdev = devices::where("name",$key['device'])->where("brand",$brand->id)->first();
                if(!$checkdev){
                    $cs = new Client();
                    $rs = $cs->get($key['hyperlink']);
                    $bd = $rs->getBody();
                    preg_match("/<div id=\"model-brief-specifications.*?>([^`]*?)<\/div>/", $bd, $bsed);
                    $sed = explode("<b>",$bsed[0]);
                    foreach ($sed as $kpe) {
                        $kpe = str_replace("Add for comparison","",$kpe);
                        $kpe = str_replace("Suggest an edit","",$kpe);
                        $kpe = str_replace("\r\n ","",$kpe);
                        $kpe = explode(":",trim(strip_tags($kpe)));
                        $kpe = array_values((array_filter($kpe)));
                        if(isset($kpe[0])){
                            $caklc[$key['device']][$kpe[0]] = $kpe[1];
                        }
                    }
                }

            }
            foreach ($caklc as $key) {
                $devicesera = devices::where("name",$key['device'])->where("brand",$key['brand'])->first();
                if(!$devicesera){
                    $newitem                = new devices;
                    $newitem->name          = (@$key['device']) ? $key['device']:"Not Available";
                    $newitem->brand         = (@$key['brand']) ? $key['brand']:"Not Available";
                    $newitem->url           = (@$key['hyperlink']) ? $key['hyperlink']:"Not Available";
                    $newitem->dimentions    = (@$key['Dimensions']) ? $key['Dimensions']:"Not Available";
                    $newitem->weight        = (@$key['Weight']) ? $key['Weight']:"Not Available";
                    $newitem->soc           = (@$key['SoC']) ? $key['SoC']:"Not Available";
                    $newitem->cpu           = (@$key['CPU']) ? $key['CPU']:"Not Available";
                    $newitem->gpu           = (@$key['GPU']) ? $key['GPU']:"Not Available";
                    $newitem->ram           = (@$key['RAM']) ? $key['RAM']:"Not Available";
                    $newitem->storage       = (@$key['Storage']) ? $key['Storage']:"Not Available";
                    $newitem->memory        = (@$key['Memory cards']) ? $key['Memory cards']:"Not Available";
                    $newitem->display       = (@$key['Display']) ? $key['Display']:"Not Available";
                    $newitem->battery       = (@$key['Battery']) ? $key['Battery']:"Not Available";
                    $newitem->os            = (@$key['OS']) ? $key['OS']:"Not Available";
                    $newitem->camera        = (@$key['Camera']) ? $key['Camera']:"Not Available";
                    $newitem->wifi          = (@$key['Wi-Fi']) ? $key['Wi-Fi']:"Not Available";
                    $newitem->usb           = (@$key['USB']) ? $key['USB']:"Not Available";
                    $newitem->bluetooth     = (@$key['Bluetooth']) ? $key['Bluetooth']:"Not Available";
                    $newitem->positioning   = (@$key['Positioning']) ? $key['Positioning'] :"Not Available";
                    $newitem->save();
                }
            }
        }
        return redirect()->route('devices.index')->with(['status'=>"Database Updated!"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show(devices $devices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function edit(devices $devices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices)
    {
        //
    }
}
