@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>Devices of {{$brand->title}}</h1>
            <a href="{{route('brands.download',[$brand->id])}}" class="btn btn-success float-right">Download</a>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <table class="table table-sm table-bordered">
                        <thead class="table-dark">
                            <tr>
                                <th style="width:20%">Name</th>
                                <th>Dimentions</th>
                                <th>Weight</th>
                                <th>Soc</th>
                                <th>CPU</th>
                                <th>GPU</th>
                                <th>Display</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($brand->devices as $device)
                                <tr>
                                    <th class="">{{$brand->title}} {{$device->name}}</th>
                                    <th>{{$device->dimentions}}</th>
                                    <th>{{$device->weight}}</th>
                                    <th>{{$device->soc}}</th>
                                    <th>{{$device->cpu}}</th>
                                    <th>{{$device->gpu}}</th>
                                    <th>{{$device->display}}</th>
                                    <th><a href="{{$device->url}}" class="btn btn-sm btn-primary" target="_blank">View</a></th>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="9">No Devices for this Brand found</th>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection