@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>Brands</h1>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-8">
                <div class="card">
                    <table class="table table-sm">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Brand Name</th>
                                <th>URL</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($brands as $brand)
                                <tr>
                                    <th>{{$brand->id}}</th>
                                    <th><a href="{{route('brands.show',[$brand->id])}}">{{$brand->title}}</a></th>
                                    <td><a href="{{$brand->url}}" target="_blank">{{$brand->url}}</a></td>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="2">No Brands found</th>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection