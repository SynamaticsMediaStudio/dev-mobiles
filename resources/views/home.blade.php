@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Help</p>
                    <p>How to:</p>
                    <p>
                        <ul>
                            <li><b>Update Brands</b>: Click on Update Brands </li>
                            <li><b>Update Devices</b>: Click on Update Devices, it will run for a few mins or maybe an hour.</li>
                            <li><b>Download All Devices</b>: Click on Devices->Download All, it will download all the devices.</li>
                            <li><b>Download Only Updated Devices</b>: Click on Devices->Download Updated only, it will download only the devices, that are updated after the last Download.</li>
                            <li><b>Adding a Cron Job</b>: <br>
                                <code class="language-html" data-lang="html">
                                    <span class="s">* * * * * cd <b>{{base_path()}}</b> && php artisan schedule:run >> /dev/null 2>&1</span>
                                </code>
                            </li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
