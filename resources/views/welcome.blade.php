    $url = "https://www.devicespecifications.com/en/brand-more";
    $client = new \GuzzleHttp\Client();
    $result = $client->get($url);
    $body = $result->getBody();
    $match = "";
    if (preg_match('#<div class="brand-listing-container-news"[^>]*>(.*?)</div>#', $body, $match) == 1) {
        $match = $match[1];
    }
    $links = [];
    preg_match_all('#<a[^>]*>(.*?)</a>#', $match, $alinks);
    foreach ($alinks[0] as $key) {
        $a  = new SimpleXMLElement($key);
        $de = $a;
        $links[] = ['brand'=>$a->__toString(),"hyperlink"=>$a['href']->__toString()];
    }