@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>All Devices</h1>
            <div class="btn-group float-right">
                <a href="{{route('brands.download',["all"])}}" class="btn btn-success">Download All</a>
                <a href="{{route('brands.download',['updated'])}}" class="btn btn-success">Download Updated Only</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <table class="table table-sm table-bordered">
                        <thead class="table-dark">
                            <tr>
                                <th style="width:20%">Name</th>
                                <th>Dimentions</th>
                                <th>Weight</th>
                                <th>Soc</th>
                                <th>CPU</th>
                                <th>GPU</th>
                                <th>Display</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($devices as $device)
                                <tr>
                                    <th class="">{{$device->Brand->title}} {{$device->name}}</th>
                                    <th>{{$device->dimentions}}</th>
                                    <th>{{$device->weight}}</th>
                                    <th>{{$device->soc}}</th>
                                    <th>{{$device->cpu}}</th>
                                    <th>{{$device->gpu}}</th>
                                    <th>{{$device->display}}</th>
                                    <th><a href="{{$device->url}}" class="btn btn-sm btn-primary" target="_blank">View</a></th>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="9">No Devices for this Brand found</th>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection