<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/brands', 'BrandsController');
Route::resource('/devices', 'DevicesController');
Route::get('/devices/download/{brand}', 'BrandsController@download')->name('brands.download');

Route::get('/installer', 'Installation@index');
Route::post('/installer', 'Installation@create')->name("installer");
Route::get('/installer/create', 'Installation@update')->name("install.create");